package hnct.lib.gradle;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class LanguageConfigurationFactory {

    Set<String> supportedLanguages;

    public LanguageConfigurationFactory() {
        supportedLanguages = new HashSet<>(Arrays.asList("scala", "java"));
    }

    public LanguageConfiguration create(String s) {

        if (!supportedLanguages.contains(s)) throw new RuntimeException("The language "+s+" is not supported");

        switch (s) {
            case "scala":
                return new ScalaLanguageConfiguration(s);
            default:
                return new LanguageConfiguration(s);
        }
    }

}
