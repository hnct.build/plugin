package hnct.lib.gradle;

import org.gradle.api.Action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DslHnct extends DslBase {

    public static final String CMD = "hnct";
    public static final String CMD_LANG = "lang";

    public DslHnct(HNCTPlugin plugin) {
        super(plugin, CMD);
    }

}
