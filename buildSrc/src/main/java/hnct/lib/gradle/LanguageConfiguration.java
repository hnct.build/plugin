package hnct.lib.gradle;

public class LanguageConfiguration {
    private String name;
    private String version;

    public LanguageConfiguration(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void configure(HNCTPlugin plugin) {
        plugin.applyPlugin(this.getName());
    }

}
