package hnct.lib.gradle;

import org.gradle.api.Project;
import org.gradle.api.tasks.SourceSet;

public class ScalaLanguageConfiguration extends LanguageConfiguration {

	private String version;

	public ScalaLanguageConfiguration(String name) {
		super(name);
	}


	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public void configure(HNCTPlugin plugin) {
		super.configure(plugin);

		Project p = plugin.getProject();
		p.container(SourceSet.class).all(s -> {
			// add scala library dependency to help infer compiler version for the scala compile task of the
			// source set. The scala plugin already configure an action to do the inference, so we just need
			// to add the dependency.
			if (s.getName() != "main") {
				p.getDependencies().add(
						s.getName()+"Compile",
						String.format("org.scala-lang:scala-library:%s", getVersion()));
			} else {
				p.getDependencies().add(
						"compile",
						String.format("org.scala-lang:scala-library:%s", getVersion()));
			}
		});

	}

}
