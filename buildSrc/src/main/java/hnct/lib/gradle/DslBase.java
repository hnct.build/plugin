package hnct.lib.gradle;

abstract public class DslBase {

    /**
     * The plugin instance that all DSL need to refer to
     */
    protected HNCTPlugin plugin;
    private String cmd;

    public DslBase(HNCTPlugin plugin, String cmd) {
        this.plugin = plugin;
        this.cmd = cmd;
    }

    public HNCTPlugin getPlugin() {
        return plugin;
    }
}
