package hnct.lib.gradle;

import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.ConfigurationContainer;
import org.gradle.api.artifacts.Dependency;
import org.gradle.api.artifacts.dsl.DependencyHandler;
import org.gradle.api.tasks.SourceSet;
import org.gradle.internal.metaobject.DynamicInvokeResult;
import org.gradle.internal.metaobject.MethodAccess;
import org.gradle.internal.metaobject.MethodMixIn;

import java.util.*;
import java.util.stream.Collectors;

/**
 * DSL extension on a project' DependencyHandler. This allows configure internal dependency among projects.
 *
 * Syntax (within individual project gradle file):
 *
 *      <pre>
 *          dependencies {
 *              internal {
 *                  _sourceset-configuration-name_(
 *                      _map-dependency-notation_ | _project-id_,
 *                      _jar-dependency-notation_,
 *                      _use-source-or-jar_
 *                  )
 *              }
 *          }
 *
 *          _map-dependency-notation_ = [
 *              'id': _project-id_, 'srcSetNames':[_sourceset-configuration-name-string_+]
 *          ]
 *
 *          _project-id_ : string of a full path to a project within the build
 *
 *          _jar-dependency-notation_ : a normal jar dependency notation, containing group, jar name, version, etc...
 *
 *          _user-source-or-jar_ : true or false. If true, the project within the build will be used, if false, use the jar
 *          from t he _jar-dependency-notation_
 *      </pre>
 *
 *
 *
 */
public class DslInternalDependency extends DslBase implements MethodMixIn, MethodAccess {

    public static final String CMD = "internal";

    private ConfigurationContainer cc;
    private DependencyHandler dh;

    public DslInternalDependency(HNCTPlugin plugin) {
        super(plugin, CMD);

        cc = plugin.getProject().getConfigurations();
        dh = plugin.getProject().getDependencies();
    }

    @Override
    public MethodAccess getAdditionalMethods() {
        return this;
    }

    @Override
    public boolean hasMethod(String name, Object... arguments) {
        return arguments.length != 3 && cc.findByName(name) != null;
    }

    private List<Dependency> parseProjDeps(Project dp, String confName, Object notation) {
        if (notation instanceof String) {   // has to be a projectId
            return parseProjDepsFromId(dp, confName, notation);
        } else return parseProjDepsFromMap(dp, confName, notation);
    }

    private List<Dependency> parseProjDepsFromMap(Project dependencyProject, String confName, Object notation) {
        Map<String, Object> m = (Map<String, Object>)notation;
        String projectId = (String)m.get("id");
        Object srcSetNames = m.get("src");

        if (srcSetNames == null) m.get("conf"); // backward compatibility

        List<String> srcs;

        if (srcSetNames instanceof String) srcs = Arrays.asList((String) srcSetNames);
        else srcs = (List<String>) srcSetNames;

        List<SourceSet> sourceSets = srcs.stream().map(sn -> Utils.findSourceSet(dependencyProject, sn))
                .collect(Collectors.toList());

        ArrayList<Dependency> result = new ArrayList<>();

        sourceSets.stream().forEach(s -> {

            Configuration targetConfig = Utils.findMatchingConfiguration(getPlugin().getProject(), dependencyProject, confName, s);

            Map<String,Object> am = new HashMap<>();
            am.put("path", projectId);
            am.put("configuration", targetConfig.getName());

            result.add(dh.project(am));
        });

        return result;
    }

    private List<Dependency> parseProjDepsFromId(Project dp, String confName, Object notation) {

        Map<String, String> notationMap = new HashMap<>();
        notationMap.put("id", (String)notation);
        notationMap.put("src", "main"); // if we depend on a project, we depends on its main source set

        return parseProjDepsFromMap(dp, confName, notationMap);

        /*String projectId = (String) notation;

        Map<String,Object> m = new HashMap<>();
        m.put("path", projectId);

        return Arrays.asList(dh.project(m));*/
    }

    @Override
    public DynamicInvokeResult tryInvokeMethod(String name, Object... arguments) {

        try {
            String jarDepNotation = arguments[1].toString();
            boolean isProjDep = (boolean) arguments[2];

            if (isProjDep) {
                Project dependencyProject = parseProjFromNotation(arguments[0]);

                if (dependencyProject == null) {
                    throw new RuntimeException("Unable to parse dependency project from "+arguments[0]+". Please check if the project is defined in your settings.gradle file.");
                }

                // since dependency is among projects which requires sourcesets to be setup properly
                // before dependency can be added, we wait for ALL projects to finish evaulation
                // before finalizing the dependency
                RunOnceUponEvaluated.instance().afterEvaluated(dependencyProject, dp -> {

                    List<Dependency> dependencies = parseProjDeps(dependencyProject, name, arguments[0]);

                    for (Dependency dependency : dependencies) {
                        dh.add(name, dependency);    // this add the dependency to the project
                    }

                    // all other dependency on the jar of the project should be substituted with the project itself
                    Objects.requireNonNull(cc.findByName(name)).resolutionStrategy(s -> s.dependencySubstitution(ds -> {
                        ds.substitute(
                                ds.module(String.format("%s:%s", dependencyProject.getGroup().toString(), dependencyProject.getName()))
                        ).using(ds.project(dependencyProject.getPath()));
                    }));
                });

            } else dh.add(name, jarDepNotation);

            return DynamicInvokeResult.found();

        } catch (ClassCastException exception) {
            return DynamicInvokeResult.notFound();
        }
    }

    private Project parseProjFromNotation(Object argument) {
        if (argument instanceof String) return getPlugin().getProject().findProject((String)argument);
        else {
            Map<String, Object> m = (Map<String, Object>)argument;
            String projectId = (String)m.get("id");

            Project dependencyProject = getPlugin().getProject().findProject(projectId);

            if (dependencyProject == null) {
                throw new RuntimeException(String.format(
                        "Unable to find project with id %s "+
                                "while adding internal dependency in project %s",
                        projectId, getPlugin().getProject().getPath())
                );
            }

            return dependencyProject;
        }
    }
}
