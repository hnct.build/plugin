package hnct.lib.gradle;

import org.gradle.api.Project;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class GitUtil {

    private static String GIT_EXECUTABLE = System.getProperty("os.name").toLowerCase().contains("windows")? "git.exe" : "git";

    public static List<String> git(Project p, String[] cmd) {
        try {
            List<String> l = new ArrayList<>(Arrays.asList(GIT_EXECUTABLE));
            l.addAll(Arrays.asList(cmd));

            ProcessBuilder pb = new ProcessBuilder();
            Process proc = pb.command(l).directory(p.getProjectDir()).start();

            BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));

            BufferedReader erbr = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

            List<String> output = br.lines().collect(Collectors.toList());

            int exitCode = proc.waitFor();

            if (exitCode != 0) {

                String gitError = erbr.lines().reduce("", (s1, s2) -> s1 + "\n" + s2);

                throw new GitException(exitCode, "Unable to execute git!", null, gitError);
            }

            return output;

        } catch (GitException ge) {
            throw ge;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public static String getDistJarAppendix(Project p) {
        List<String> gitResults = new ArrayList<>();
        String gitCommitHash, gitCommitBranch;

        try {

            // Since these commands above ony return one value
            // Therefore we will get the result and  convert it to a string
            gitResults = git(p, new String[]{"rev-parse", "--short=20", "--verify", "HEAD"});
            gitCommitHash = String.join("", gitResults);

            gitResults = git(p, new String[]{"rev-parse", "--abbrev-ref", "--verify", "HEAD"});
            gitCommitBranch = String.join("", gitResults);

        } catch (GitException e) {
            System.out.println(String.format(
                    "Unable to initialize appendices for dist tasks. " +
                            "All dist jar name will not have git hash attached. " +
                            "Git execution exit with code %d and with error:\n%s",
                    e.exitCode,
                    e.error
            ));

            return "";
        }

        StringBuffer sb = new StringBuffer();
        
        if (gitCommitHash.isEmpty()) return "";

        String branchName = ((System.getenv("GIT_COMMIT_BRANCH") != null) ? System.getenv("GIT_COMMIT_BRANCH") : gitCommitBranch);
        
        sb.append(branchName).append("-").append(gitCommitHash);

        return sb.toString();
    }

}
