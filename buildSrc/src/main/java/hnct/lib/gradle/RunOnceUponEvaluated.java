package hnct.lib.gradle;

import org.gradle.api.Action;
import org.gradle.api.Project;
import org.gradle.api.invocation.Gradle;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class RunOnceUponEvaluated {

    private static RunOnceUponEvaluated inst;

    private Map<Project, List<Action<? super Project>>> afterEvaluatedActions;
    private Map<Project, AtomicBoolean> isEvaluated;
    private Gradle g;

    private RunOnceUponEvaluated() {
        afterEvaluatedActions = new ConcurrentHashMap<Project, List<Action<? super Project>>>(10, 0.7f, 1);
        isEvaluated = new ConcurrentHashMap<Project, AtomicBoolean>(10, 0.7f, 1);
    }

    public static RunOnceUponEvaluated instance() {
        if (inst == null) inst = new RunOnceUponEvaluated();

        return inst;
    }

    public void register(Project p) {

        if (g == null) {
            g = p.getGradle();

            g.projectsEvaluated(this::cleanUp);
        }

        afterEvaluatedActions.put(p, new ArrayList<>());
        isEvaluated.put(p, new AtomicBoolean(false));

        p.afterEvaluate(this::projectEvaluated);
    }

    private void cleanUp(Gradle gradle) {

        isEvaluated.clear();

        afterEvaluatedActions.clear();

    }

    public void afterEvaluated(Project p, Action<? super Project> action) {

        AtomicBoolean status = isEvaluated.get(p);

        if (status == null) {
            // this probably means the project p is not evaluated before,
            // and we just use its normal afterEvaluate method
            p.afterEvaluate(action);

            return;
        }

        synchronized(status) {

            if (status.get()) action.execute(p);
            else {
                List<Action<? super Project>> actionList = afterEvaluatedActions.computeIfAbsent(p, proj -> new ArrayList<>());
                actionList.add(action);
            }
        }
    }

    private void projectEvaluated(Project project) {

        AtomicBoolean status = isEvaluated.get(project);

        if (status == null) throw new RuntimeException("You cannot use this run once upon evaluated service if not register the project earlier.");

        synchronized (status) {

            status.set(true);

            List<Action<? super Project>> actionList = afterEvaluatedActions.get(project);

            actionList.forEach(a -> a.execute(project));

        }
    }

}
