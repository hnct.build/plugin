package hnct.lib.gradle;

import groovy.lang.Closure;
import org.codehaus.groovy.runtime.InvokerHelper;
import org.gradle.api.Action;
import org.gradle.api.Project;
import org.gradle.api.artifacts.dsl.RepositoryHandler;
import org.gradle.api.artifacts.repositories.ArtifactRepository;
import org.gradle.api.artifacts.repositories.IvyArtifactRepository;
import org.gradle.api.artifacts.repositories.MavenArtifactRepository;

import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

public class DslRepo extends DslBase {

    public static final String CMD = "repositories";

    public DslRepo(HNCTPlugin plugin) {
        super(plugin, CMD);

        // clear all the repositories preconfigured for the projects by gradle
        if (plugin.getProject().getRootProject() == plugin.getProject())
            plugin.getProject().getRepositories().clear();

        plugin.getProject().subprojects(p -> {
            p.getRepositories().clear();
        });

    }

    public void maven(Object args) {
        Map<String, Object> al = (Map<String, Object>)args;
        al.put("type", RepositoryConfiguration.MAVEN);
        custom(al);
    }

    public void ivy(Object args) {
        Map<String, Object> al = (Map<String, Object>)args;
        al.put("type", RepositoryConfiguration.IVY);
        custom(al);
    }

    private RepositoryConfiguration configFromMap(Object args) {
        RepositoryConfiguration conf = new RepositoryConfiguration();
        Map<String, Object> props = (Map<String, Object>)args;

        InvokerHelper.setProperties(conf, props);

        return conf;
    }

    public void custom(Object args) {
        addRepo(configFromMap(args));
    }

    private Function<Action<ArtifactRepository>, ArtifactRepository> getRepositoryCreator(String type, Project p) {
        switch (type) {
            case RepositoryConfiguration.IVY:
                return p.getRepositories()::ivy;
            case RepositoryConfiguration.MAVEN:
                return p.getRepositories()::maven;
            default:
                return null;
        }
    }

    private BiConsumer<ArtifactRepository, RepositoryConfiguration> getRepositorySetter(String type) {
        switch (type) {
            case RepositoryConfiguration.IVY:
                return this::configureIvyRepository;
            case RepositoryConfiguration.MAVEN:
                return this::configureMavenRepository;
            default:
                return null;
        }
    }

    private void configureMavenRepository(ArtifactRepository repo, RepositoryConfiguration config) {
        MavenArtifactRepository r = (MavenArtifactRepository)repo;
        r.setName(config.getName());
        r.setUrl(config.getUrl());

        if (config.getUsername() != null) {
            r.credentials(c -> {
                c.setUsername(config.getUsername());
                c.setPassword(config.getPassword());
            });
        }
    }

    private void configureIvyRepository(ArtifactRepository repo, RepositoryConfiguration config) {
        IvyArtifactRepository r = (IvyArtifactRepository) repo;
        r.setName(config.getName());
        r.setUrl(config.getUrl());

        if (config.getUsername() != null) {
            r.credentials(c -> {
                c.setUsername(config.getUsername());
                c.setPassword(config.getPassword());
            });
        }
    }

    private void addRepo(RepositoryConfiguration rc) {

        Function<Action<ArtifactRepository>, ArtifactRepository> f = getRepositoryCreator(rc.getType(), getPlugin().getProject());
        BiConsumer<ArtifactRepository, RepositoryConfiguration> s = getRepositorySetter(rc.getType());

        if (f == null || s == null) throw new RuntimeException("Unsupported repository type: "+rc.getType()+" for repository named: "+rc.getName());

        f.apply(ar -> s.accept(ar, rc));

        if (rc.isAppliedToChildren())
            getPlugin().getProject().subprojects(p -> {
                Function<Action<ArtifactRepository>, ArtifactRepository> fsub = getRepositoryCreator(rc.getType(), p);

                if (fsub == null || s == null) throw new RuntimeException("Unsupported repository type: "+rc.getType()+" for repository named: "+rc.getName());

                fsub.apply(ar -> s.accept(ar, rc));
            });
    }

    public void custom(Closure configurationClosure) {
        RepositoryConfiguration rc = (RepositoryConfiguration)getPlugin().getProject().configure(new RepositoryConfiguration(), configurationClosure);
        addRepo(rc);
    }

    public void jayeson(Object args) {
        RepositoryConfiguration conf = configFromMap(args);

        String url = conf.getUrl();

        conf.setType(RepositoryConfiguration.MAVEN);
        conf.setName("lib-release");
        conf.setUrl(url+"/"+conf.getName());
        addRepo(conf);

        conf.setType(RepositoryConfiguration.IVY);
        conf.setName("production");
        conf.setUrl(url+"/"+conf.getName());
        addRepo(conf);

        if (!getPlugin().isDistributing() || getPlugin().isDistDev()) {
            conf.setType(RepositoryConfiguration.IVY);
            conf.setName("snapshot");
            conf.setUrl(url+"/"+conf.getName());
            addRepo(conf);
        } else {
            System.out.println("************ WARNING ***********");
            System.out.println("\nDistribution disable ALL Jayeson's snapshot repositories. IF YOU REALLY NEED TO DEPLOY SNAPSHOT, USE distDev[SourceSetName] TASKS INSTEAD\n");
            System.out.println("********************************");
        }
    }

    public void local() {

        RepositoryConfiguration conf = new RepositoryConfiguration();

        conf.setName("local-repo");
        conf.setType(RepositoryConfiguration.IVY);
        conf.setUrl(getPlugin().getProject().getRootDir().getParentFile().getAbsolutePath()+"/.ivy");

        addRepo(conf);
    }

    public void local(String url) {
        RepositoryConfiguration conf = new RepositoryConfiguration();

        conf.setName("local-repo");
        conf.setType(RepositoryConfiguration.IVY);
        conf.setUrl(url);

        addRepo(conf);
    }

    public void standard() {
        local();

        RepositoryHandler handler = getPlugin().getProject().getRepositories();
        handler.mavenCentral();
        //handler.jcenter();
        handler.google();

        getPlugin().getProject().subprojects(p -> {
            RepositoryHandler subHandler = p.getRepositories();
            subHandler.mavenCentral();
            //subHandler.jcenter();
            subHandler.google();
        });
    }
}
