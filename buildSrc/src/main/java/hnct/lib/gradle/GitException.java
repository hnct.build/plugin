package hnct.lib.gradle;

public class GitException extends RuntimeException {

	public final String error;
	public final int exitCode;

	public GitException(int exitCode, String message, Throwable cause, String error) {
		super(message, cause);
		this.error = error;
		this.exitCode = exitCode;
	}

}
