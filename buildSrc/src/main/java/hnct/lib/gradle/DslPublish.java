package hnct.lib.gradle;

import org.gradle.StartParameter;
import org.gradle.api.Action;
import org.gradle.api.tasks.SourceSetContainer;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.Dependency;
import org.gradle.api.artifacts.FileCollectionDependency;
import org.gradle.api.artifacts.ProjectDependency;
import org.gradle.api.artifacts.dsl.RepositoryHandler;
import org.gradle.api.artifacts.repositories.ArtifactRepository;
import org.gradle.api.artifacts.repositories.IvyArtifactRepository;
import org.gradle.api.plugins.JavaPluginExtension;
import org.gradle.api.publish.PublicationContainer;
import org.gradle.api.publish.PublishingExtension;
import org.gradle.api.publish.ivy.IvyPublication;
import org.gradle.api.publish.ivy.plugins.IvyPublishPlugin;
import org.gradle.api.publish.ivy.tasks.PublishToIvyRepository;
import org.gradle.api.publish.plugins.PublishingPlugin;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.TaskContainer;
import org.w3c.dom.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.function.Function;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Currently only support IVY Repo!!!
 */
public class DslPublish extends DslBase {

    public static final String CMD = "publish";
    private static final String publishSummaryFile = "publishSummary.txt";
    private final RepositoryHandler snapshotRepo;
    private final RepositoryHandler productionRepo;

    private boolean isPublishDev, isPublishProd, isPublishDeployDev, isPublishDeployProd;

    public DslPublish(HNCTPlugin plugin, Project p, RepositoryHandler snapshot, RepositoryHandler productionRepo) {
        super(plugin, CMD);

        this.snapshotRepo = snapshot;
        this.productionRepo = productionRepo;

        if (p.getRootProject() != p) throw new RuntimeException("Publishing repository can only be configured at root");

        boolean isListingTask = doesTaskContain(p, "tasks");
        isPublishDev = doesTaskContain(p, "publishDev") || isListingTask;
        isPublishDeployDev = doesTaskContain(p, "publishDeployDev") || isListingTask;

        isPublishProd = doesTaskContain(p, "publishProd") || isListingTask;
        isPublishDeployProd = doesTaskContain(p, "publishDeployProd") || isListingTask;

        p.subprojects(sub -> {

            sub.getPluginManager().apply(IvyPublishPlugin.class);

            sub.afterEvaluate(project -> {
                configurePublicationForProject(project);
            });
        });

    }

    /**
     * Configure publication for a particular project. One dev and one prod publication for a sourceset, except
     * for test and integration test
     * @param p the project
     */
    public void configurePublicationForProject(Project p) {
        PublishingExtension e = p.getExtensions().findByType(PublishingExtension.class);

        DslHnct hnct = p.getExtensions().findByType(DslHnct.class);

        if (p.getExtensions().findByType(JavaPluginExtension.class) == null) {
            System.out.println("Project "+p.getName()+" doesn't have Java Plugin Applied! No publication will be configured.");
            return;   // currently only support java convention
        }

        if (e != null && hnct != null) {
            SourceSetContainer srcSets = p.getExtensions().getByType(SourceSetContainer.class);

            TaskContainer tasks = p.getTasks();

            tasks.register("publishDev", publishDev -> {
                publishDev.setDescription("Publish all development publication of all projects to all repos");
                publishDev.setGroup(PublishingPlugin.PUBLISH_TASK_GROUP);
            });

            tasks.register("publishProd", publishProd -> {
                publishProd.setDescription("Publish all production publication of all projects to all repos");
                publishProd.setGroup(PublishingPlugin.PUBLISH_TASK_GROUP);
            });

            tasks.register("publishDeployDev", publishDeployDev -> {
                publishDeployDev.setDescription("Publish all development publication of all projects to all repos for future deployment");
                publishDeployDev.setGroup(PublishingPlugin.PUBLISH_TASK_GROUP);
            });

            tasks.register("publishDeployProd", publishDeployProd -> {
                publishDeployProd.setDescription("Publish all production publication of all projects to all repos for future deployment");
                publishDeployProd.setGroup(PublishingPlugin.PUBLISH_TASK_GROUP);
            });

            // for each development repository, create a publishing task to
            // publish ALL the development publication to that repo
            snapshotRepo.all(repository -> {

                tasks.register(publishToDevRepoTaskName(repository), publish -> {
                    publish.setDescription("Publishes all development publication to the " + repository.getName() + " repository.");
                    publish.setGroup(PublishingPlugin.PUBLISH_TASK_GROUP);
                });

                tasks.named("publishDev", t -> t.dependsOn(publishToDevRepoTaskName(repository)));
                tasks.named("publishDeployDev", t -> t.dependsOn(publishToDevRepoTaskName(repository)));
            });

            productionRepo.all(repository -> {

                tasks.register(publishToProdRepoTaskName(repository), publish -> {
                    publish.setDescription("Publishes all production publication to the " + repository.getName() + " repository.");
                    publish.setGroup(PublishingPlugin.PUBLISH_TASK_GROUP);
                });

                tasks.named("publishProd", t -> t.dependsOn(publishToProdRepoTaskName(repository)));
                tasks.named("publishDeployProd", t -> t.dependsOn(publishToProdRepoTaskName(repository)));
            });

            srcSets.all(s ->
                e.publications(publications -> {

                    if (isPublishDev || isPublishDeployDev) {
                        // configure development publication
                        IvyPublication devIvyPub = generatePublicationForSourceSet(s, p, publications, true, (isPublishDeployDev)? true : false);
                        // for each development publication,
                        // create the publishing task for it to the development repository
                        // and  make the publish task of the repository depends on that task
                        if (devIvyPub != null)
                            createPublishTasks(p, tasks, devIvyPub, snapshotRepo, this::publishToDevRepoTaskName);
                    }

                    if (isPublishProd || isPublishDeployProd) {
                        IvyPublication prodIvyPub = generatePublicationForSourceSet(s, p, publications, false, (isPublishDeployProd)? true : false);
                        // for each production publication,
                        // create the publishing task for it to the production repository
                        // and  make the publish task of the repository depends on that task
                        if (prodIvyPub != null)
                            createPublishTasks(p, tasks, prodIvyPub, productionRepo, this::publishToProdRepoTaskName);
                    }
                })
            );

        } else {
            System.out.println("Project "+p.getName()+" doesn't have HNCT plugin applied or not having publishing plugin applied. No publication is configured.");
        }
    }

    private boolean doesTaskContain(Project p, String taskName) {
        StartParameter sps = p.getGradle().getStartParameter();
        Iterator<String> it = sps.getTaskNames().iterator();
        while (it.hasNext()) {
            if (it.next().contains(taskName)) return true;
        }

        return false;
    }

    private String publishToDevRepoTaskName(ArtifactRepository repository) {
        return "publishDevTo"+Utils.capitalize(repository.getName())+"Repo";
    }

    private String publishToProdRepoTaskName(ArtifactRepository repository) {
        return "publishProdTo"+Utils.capitalize(repository.getName())+"Repo";
    }

    /**
     * Create publishing tasks for a publication in all repositories configured by a repository handler
     * @param tasks task container
     * @param ivyPub the publication to be published
     * @param repoHandler repository handler
     */
    private void createPublishTasks(Project p, TaskContainer tasks, IvyPublication ivyPub, RepositoryHandler repoHandler, Function<ArtifactRepository, String> dependingTaskName) {
        repoHandler.all(repo -> {
            String taskName = "publish"+Utils.capitalize(ivyPub.getName())+"To"+Utils.capitalize(repo.getName())+"Repo";
            tasks.register(taskName, PublishToIvyRepository.class, publishTask -> {
                publishTask.setPublication(ivyPub);
                publishTask.setRepository((IvyArtifactRepository)repo);
                publishTask.setGroup(PublishingPlugin.PUBLISH_TASK_GROUP);
                publishTask.setDescription("Publishes Ivy publication '" + ivyPub.getName() + "' to Ivy repository '" + repo.getName() + "'.");

                // This will export the current published version to build folder
                publishTask.doLast(new Action<Task>() {
                    @Override
                    public void execute(Task task) {
                        writePublishVersionToFile(p.getBuildDir().getAbsolutePath()+"/" + publishSummaryFile, ivyPub.getRevision());
                    }
                });
            });

            tasks.named(dependingTaskName.apply(repo), publish -> publish.dependsOn(taskName));
        });

    }

    public IvyPublication generatePublicationForSourceSet(SourceSet s, Project p, PublicationContainer publications, boolean isDev, boolean isPublishForDeploy) {

        Configuration c = s.getName().equals("main")?
                p.getConfigurations().findByName("runtimeClasspath"):
                p.getConfigurations().findByName(s.getName()+"RuntimeClasspath");

        Task j = s.getName().equals("main") ?
                p.getTasks().findByName("jar") :
                p.getTasks().findByName(String.format("jar%s", Utils.capitalize(s.getName())));

        if (j == null) {
            j = p.getTasks().findByName(String.format("%sJar", s.getName()));
        }

        Task jarTask = j;

        if (c != null && jarTask != null) {

            return publications.create((isDev?"Dev":"Prod")+Utils.capitalize(s.getName()), IvyPublication.class, publication -> {

                publication.artifact(jarTask);

                // For play project, publish assets file with the source porject
                if (plugin.isPlayApplied(p) && s.getName().equals("main")) {

                    Task assetsJarTask = p.getTasks().findByName("createPlayAssetsJar");

                    publication.artifact(assetsJarTask, assetsArtifact -> {
                        assetsArtifact.setType("assets");
                        assetsArtifact.setClassifier("assets");
                        assetsArtifact.setExtension("jar");
                    });
                }

                if (!s.getName().equals("main")) publication.setModule(p.getName() + "-" + s.getName());

                publication.setRevision(isDev?devRevision(p):p.getVersion().toString());

                publication.getDescriptor().withXml(xp -> {

                    Element root = xp.asElement();

                    if(isPublishForDeploy) writeConfigurationForPublication(c, p, root);

                    writeDependencyForPublication(s, c, p, root, isDev, isPublishForDeploy);

                });

            });
        }

        return null;

    }

    private void writeDependencyForPublication(SourceSet s, Configuration c, Project p, Element root, boolean isDev, boolean isPublishForDeploy) {
        Document doc = root.getOwnerDocument();
        Node dependencies = root.getElementsByTagName("dependencies").item(0);

        c.getAllDependencies().stream().forEach(d -> {
            if (d instanceof ProjectDependency) {
                if (!isDev &&
                        ((ProjectDependency) d).getDependencyProject() != p
                ) throw new RuntimeException("Project "+p.getName()+", source set "+
                        s.getName()+" has some project dependency and cannot be published to production.");
                else writeProjectDependencyNotation((ProjectDependency) d, doc, dependencies, isDev);
            } else if (d instanceof FileCollectionDependency) {
                // if this is a file collection dependency, don't include into the ivy file
            } else writeExternalDependency(d, doc, dependencies);
        });

        // Project which will be used for deploying, require specific artifact dependency version to launch
        // However, it may contain configurations that including forcing transitive dependency version or exclude some unwanted dependency
        // And Ivy module haven't support these configuration.
        // Therefore, declaring resolved dependencies for the project to ivy.xml with "transitive" mode is off
        // Gradle will not need to resolve dependencies of this project again.
        // This will keep the same version of any transitive dependencies.
        if (isPublishForDeploy) {
            writeDependencyForDeploy(c, doc, dependencies);
        }
    }

    private void writeConfigurationForPublication(Configuration c, Project p, Element root) {
        Node configurations = root.getElementsByTagName("configurations").item(0);
        
        ((Element)configurations).setAttribute("defaultconfmapping", "default");

        Document doc = root.getOwnerDocument();

        Element defaultConf = doc.createElement("conf");
        defaultConf.setAttribute("name", "default");
        defaultConf.setAttribute("description", "Default configuration for the current publish");
        configurations.appendChild(defaultConf);

        // Deploy configuration for fetching resolved dependencies only
        Element deployConf = doc.createElement("conf");
        deployConf.setAttribute("name", "deploy");
        deployConf.setAttribute("transitive", "false");
        deployConf.setAttribute("extends", "default");
        deployConf.setAttribute("description", "Deploy configuration for future deployment");
        configurations.appendChild(deployConf);
    }

    private String devRevision(Project p) {

        String jarAppendix = p.property(HNCTPlugin.DIST_JAR_APPENDIX).toString();

        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");

        Date d = new Date();
        String datetime = df.format(d);

        if (jarAppendix != null && !jarAppendix.isEmpty()) {
            String hash = jarAppendix.split("-")[1];

            return p.getVersion()+"."+datetime+"."+hash;
        } else return p.getVersion()+"."+datetime;
    }

    private void writeExternalDependency(Dependency d, Document doc, Node dependencies) {

        Element dep = doc.createElement("dependency");
        dep.setAttribute("org", d.getGroup());
        dep.setAttribute("name", d.getName());
        dep.setAttribute("rev", d.getVersion());

        dependencies.appendChild(dep);

    }

    private void writeDependencyForDeploy(Configuration c, Document doc, Node dependencies) {
        List<String> addedDependencies = new ArrayList<String>();

        c.getResolvedConfiguration().getResolvedArtifacts().stream().forEach(d -> {
            String resolvedDependency = d.getModuleVersion().toString();

            if (! addedDependencies.contains(resolvedDependency)) {
                // When resolving artifact dependencies, it may contain the dependency.
                // Adding flag to avoid duplicate in the ivy file
                addedDependencies.add(resolvedDependency);

                String[] depData = resolvedDependency.split(":");

                Element dep = doc.createElement("dependency");
                dep.setAttribute("org", depData[0]);
                dep.setAttribute("name", depData[1]);
                dep.setAttribute("rev", depData[2]);
                dep.setAttribute("conf", "deploy->*");
                dependencies.appendChild(dep);

            }
        });
    }

    private void writeProjectDependencyNotation(ProjectDependency pd, Document doc, Node dependencies, boolean isDev) {
        Project dp = pd.getDependencyProject();
        String conf = pd.getTargetConfiguration();

        Element dep = doc.createElement("dependency");
        dep.setAttribute("org", dp.getGroup().toString());

        // when writing project dependencies, if it is development mode, the notation for a
        // sourceset dependency or a dependency on another project sourceset is the same
        // when it is not development mode, if we reach here, this project dependency must be
        // a dependency on a source set of the same project. We will use version of the project
        // as the revision number for the dependency notation
        if (isDev)
            dep.setAttribute("rev", devRevision(dp));
        else dep.setAttribute("rev", dp.getVersion().toString());

        if (conf == null || conf.isEmpty()) dep.setAttribute("name", dp.getName());
        else {
            // find the sourceset that this conf contains
            SourceSet s = Utils.findSourceSetMatchConfig(dp, conf);

            if (s == null) return;
            else if (s.getName().equals("main")) dep.setAttribute("name", dp.getName());
            else dep.setAttribute("name", dp.getName()+"-"+s.getName());
        }

        dependencies.appendChild(dep);

    }

    private void writePublishVersionToFile(String filePath, String ver) {
        try {
            File f = new File(filePath);
            FileWriter fw = new FileWriter(f);
            fw.write(ver);
            fw.close();        
        } 
        catch (Exception e) { 
            e.printStackTrace();
        }
    }

    public void dev(Action<? super RepositoryHandler> configure) {
        configure.execute(snapshotRepo);
    }

    public void prod(Action<? super RepositoryHandler> configure) {
        configure.execute(productionRepo);
    }

}
