package hnct.lib.gradle;

import org.gradle.api.tasks.SourceSetContainer;
import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.ConfigurationContainer;
import org.gradle.api.artifacts.dsl.DependencyHandler;
import org.gradle.api.plugins.JavaPluginExtension;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.SourceSetOutput;
import org.gradle.internal.metaobject.DynamicInvokeResult;
import org.gradle.internal.metaobject.MethodAccess;
import org.gradle.internal.metaobject.MethodMixIn;

import javax.xml.transform.Source;
import java.util.*;

/**
 * Define the DSL for configuring source sets. It supports
 * <ul>
 *     <li>Configuring dependency among source sets</li>
 * </ul>
 */
public class DslSourceSets extends DslBase implements MethodMixIn, MethodAccess {

    public static final String CMD = "sourceSets";

    private DependencyHandler dh;
    private ConfigurationContainer cc;
    private Project p;

    private Map<SourceSet, Set<SourceSet>> reverseOutputDependencies;

    public DslSourceSets(HNCTPlugin plugin) {

        super(plugin, CMD);
        dh = plugin.getProject().getDependencies();
        cc = plugin.getProject().getConfigurations();
        p = plugin.getProject();

        reverseOutputDependencies = new HashMap<>();
        
        p.afterEvaluate(p -> configureSourceSetOutputDependencies(reverseOutputDependencies));

    }

    private void configureSourceSetOutputDependencies(Map<SourceSet, Set<SourceSet>> reverseOutputDependencies) {

        reverseOutputDependencies.entrySet().forEach(e -> {
            SourceSet dependent = e.getKey();
            Set<SourceSet> origins = e.getValue();
            origins.forEach(s -> {

                dependent.getOutput().getClassesDirs().forEach(fc -> {
                    s.getOutput().dir(fc);

                    //System.out.println("Directory "+fc.getAbsolutePath()+ " has been added to sourceset "+s.getName()+"'s output");
                });
            });
        });

    }

    /**
     * Configure a source set to depend on another source set
	 * Syntax:
	 * <pre>
	 *     sourceSets {
	 *         dep [target source set] (, [dependency source sets])+
	 *     }
	 * </pre>
	 *
     * @param s the target source set whose dependencies will be listed in deps
     * @param deps the source set that is the dependency
     */
    @Deprecated
    public void dep(SourceSet s, SourceSet ... deps) {

        if (p.getExtensions().findByType(JavaPluginExtension.class) == null) {
            throw new RuntimeException("Project "+p.getName()+" doesn't have Java Plugin Applied! Source set dependency is only applicable when the project follows Java convention.");
        }

        Arrays.asList(deps).forEach(d -> {
            Map<String, Object> m = new HashMap<>();
            m.put("path", p.getPath());

            // implementation
            m.put("configuration", d.getApiElementsConfigurationName());
            dh.add(s.getImplementationConfigurationName(), dh.project(m));

            // runtime
            m.put("configuration", d.getRuntimeElementsConfigurationName());
            dh.add(s.getRuntimeClasspathConfigurationName(), dh.project(m));

        });
    }

    @Override
    public MethodAccess getAdditionalMethods() {
        return this;
    }

    @Override
    public boolean hasMethod(String name, Object... arguments) {
        return cc.findByName(name) != null;
    }

    @Override
    public DynamicInvokeResult tryInvokeMethod(String name, Object... arguments) {

        if (cc.findByName(name) == null) throw new RuntimeException("Configuration name "+name+ " is undefined.");

        try {

            for (int i = 0; i< arguments.length; i++) {

                // find the source set that the configuration specified by "name" depends on

                if (arguments[i] instanceof SourceSet) {
                    SourceSet s = (SourceSet) arguments[i];

                    // based on the configuration name, and the sourceset, find the best matched
                    // configuration that the configuration specified by [name] should depends on
                    Configuration c = Utils.findMatchingConfiguration(p, p, name, s);

                    Map<String, Object> m = new HashMap<>();
                    m.put("path", p.getPath());
                    m.put("configuration", c.getName());

                    dh.add(name, dh.project(m));
                } else if (arguments[i] instanceof SourceSetOutput) {

                    // The configuration depends on a sourceset A's output. This means that the
                    // output of the specified sourceset will be combined with the
                    // output of the sourceset corresponding to the configuration (sourcet B) when dist task is run
                    // when publish, the output of A is published within the jar of B
                    // hence there will not be entry for it within the ivy.xml file of B
                    SourceSetOutput s = (SourceSetOutput) arguments[i];

                    SourceSetContainer srcSets = p.getExtensions().getByType(SourceSetContainer.class);

                    Optional<SourceSet> src = srcSets.
                            stream().filter(s1 -> s1.getOutput() == s).findAny();

                    // based on the configuration name, and the sourceset, find the best matched
                    // configuration that the configuration specified by [name] should depends on
                    if (src.isPresent()) {
                        Configuration c = Utils.findMatchingConfiguration(p, p, name, src.get());
                        p.getConfigurations().getByName(name).extendsFrom(c);
                        // System.out.println("Configuration "+name+" extends from "+c.getName());
                    } else {
                        throw new RuntimeException("Configuration "+name+" is configured to depend on a output of a source set from another project.");
                    }

                    dh.add(name, src.get().getOutput().getClassesDirs());

                    SourceSet sorigin = Utils.findSourceSetMatchConfig(p, name);

                    //addReverseOutputDependencies(sorigin, src.get());

                    //ConfigurableFileCollection fc = (ConfigurableFileCollection)sorigin.getOutput().getDirs();
                    //fc.from(src.get().getOutput().getDirs());
                    //sorigin.setCompileClasspath(sorigin.getCompileClasspath().plus(src.get().getOutput().getDirs()));

                    // System.out.println("Output of source set "+src.get().getName()+
                    //        " has been added to the source set output of "+sorigin.getName());

                } else {
                    throw new RuntimeException("Only source set name or source set output is allowed as dependency for configuration"+
                            name + " under the sourceSets section.");
                }
            }

            return DynamicInvokeResult.found();

        } catch (ClassCastException exception) {
            return DynamicInvokeResult.notFound();
        }
    }

    private void addReverseOutputDependencies(SourceSet origin, SourceSet dependent) {

        if (dependent == origin) throw new RuntimeException("Circular sourceset output dependency for sourceset "+dependent.getName());

        Set<SourceSet> dependents = reverseOutputDependencies.computeIfAbsent(dependent, s -> new HashSet<>());

        if (!dependents.contains(origin)) {
            dependents.add(origin);

            Set<SourceSet> originDependents = reverseOutputDependencies.get(origin);

            if (originDependents != null) {
                for (SourceSet s : originDependents.toArray(new SourceSet[]{})) {
                    addReverseOutputDependencies(s, dependent);
                }
            }
        }
    }

}
