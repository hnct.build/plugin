package hnct.lib.gradle;

import groovy.lang.Closure;
import groovy.lang.MissingPropertyException;
import org.codehaus.groovy.runtime.InvokerHelper;

import java.util.Map;

public class DslLang extends DslBase {

    public static final String CMD = "lang";
    private LanguageConfigurationFactory lcf;

    public DslLang(HNCTPlugin plugin) {
        super(plugin, CMD);

        lcf = new LanguageConfigurationFactory();
    }

    public void methodMissing(String name, Object args) {
        Object[] arr = (Object[])args;

        LanguageConfiguration config = lcf.create(name);

        if (args != null && arr.length > 0) {

            try {   // trying to invoke the configuration
                Closure c = (Closure) arr[0];
                c.setDelegate(config);

                getPlugin().addLanguage((LanguageConfiguration) getPlugin().getProject().configure(config, c));

                return;
            } catch (ClassCastException ex){

            }

            try {
                Map<String, Object> m = (Map<String, Object>)arr[0];

                InvokerHelper.setProperties(config, m);

                getPlugin().addLanguage(config);

                return;
            } catch (ClassCastException ex) {
                ex.printStackTrace();
            }

            throw new RuntimeException(
                    String.format(
                            "Need a closure or a map to make a language configuration. Get input %s", InvokerHelper.inspect(args)
                    )
            );

        } else {
            getPlugin().addLanguage(config);
        }


    }

}
