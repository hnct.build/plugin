package hnct.lib.gradle;

import org.gradle.StartParameter;
import org.gradle.api.tasks.SourceSetContainer;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.dsl.RepositoryHandler;
import org.gradle.api.internal.artifacts.ArtifactPublicationServices;
import org.gradle.api.plugins.ExtensionAware;
import org.gradle.api.plugins.JavaBasePlugin;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.plugins.JavaPluginExtension;
import org.gradle.api.tasks.Copy;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.bundling.Jar;
import org.gradle.api.tasks.SourceSetContainer;

import javax.inject.Inject;
import java.io.File;
import java.util.*;

/**
 * This plugin extends Gradle DSL to provide following functionalities:
 * <ul>
 *     <li>Allow configuring a project with a number of languages for the project. Currently supporting JAVA and SCALA.</li>
 *     <li>Allow configuring a project with PLAY framework in Java or Scala.</li>
 *     <li>Read configuration and configure the projects with correct IDE plugins. Currently support Eclipse or IntelliJ.</li>
 *     <li>Setup additional source sets for the project for each specified language</li>
 *     <li>Allow configuring dependencies among source set. Meaning implementation from one source set is available to other source set when calling gradle compile</li>
 *     <li>Source sets inter-dependencies should be configured easily through specification like sourceSetDep { A [B,C,D] } which make A depends on output of B, C, D</li>
 *     <li>Allow configuring external dependencies inheritance among source set. </li>
 *     <li>Pre-configured with a number of repositories, e.g. maven central, jcenter, bintray, etc... </li>
 *     <li>Allow easy configuration of a custom repository</li>
 *     <li>Support multiple projects build with source dependency or jar dependencies among project.</li>
 *     <li>Support distribution of individual source set and combined. All distribution comes with all runtime dependencies.</li>
 *     <li>Allow force refreshing dependencies through configuration in the root project</li>
 *     <li>Support attaching git hash at the end of the distribution jar file for easy tracking commit.</li>
 * </ul>
 *
 * The DSL are extended with following syntax:
 *
 *
 *
 */
public class HNCTPlugin implements Plugin<Project> {

	public static final String DIST_JAR_APPENDIX = "distJarAppendix";
	private final ArtifactPublicationServices publicationServices;
	private Project p;
	private Map<String, LanguageConfiguration> langConfigs = new HashMap<>();

	private String jarAppendix;

	ExtensionAware sourceSets;

	private static final String DIST_FOLDER = "dist";

	private static final String DIST_TASK_GROUP = "Distribution tasks";

	Task distAll;

	/**
	 * A flag for whether source sets have already been set up for directories
	 * inside source folder. Source set setup is only applicable when at least a language
	 * is configured.
	 */
	boolean sourceSetsSetup;

	private String distTaskPrefix() {
		return this.isDistDev()? "distDev" : "dist";
	}

	@Inject
	public HNCTPlugin(ArtifactPublicationServices publicationServices) {
		this.publicationServices = publicationServices;
	}

	@Override
	public void apply(Project p) {
		this.p = p;

		RunOnceUponEvaluated.instance().register(p);

		this.jarAppendix = GitUtil.getDistJarAppendix(p);
		p.getExtensions().getExtraProperties().set(DIST_JAR_APPENDIX, this.jarAppendix);

		distAll = p.task(distTaskPrefix()+"All");	// create a dist task
		distAll.setGroup(DIST_TASK_GROUP);
		distAll.setDescription("Runs distribution task of all other source sets");

		constructDsl();
	}

	/**
	 * Setup all source sets from the source directory
	 */
	private void setupSourceSets() {

		JavaPluginExtension javaPlugin = p.getExtensions().getByType(JavaPluginExtension.class);

		if (javaPlugin != null) sourceSetsSetup = true;
		else throw new RuntimeException("The java plugin or java library plugin has not been applied properly");

		SourceSetContainer srcSets = p.getExtensions().getByType(SourceSetContainer.class);

		File srcDir = new File(p.getProjectDir().getPath()+"/src");

		if (srcDir.exists()) {
			Arrays.asList(srcDir.listFiles()).forEach(f -> {
				if (f.isDirectory() && srcSets.findByName(f.getName()) == null) {
					srcSets.create(f.getName());
				}
			});
		}

		srcSets.all(s -> {

			// if this is the main sourceset, and play plugin is applied,
			// we don't need to configure dist task for this sourceset
			if (isPlayApplied(p)) return;

			javaPlugin.registerFeature(s.getName(), spec -> {
				spec.usingSourceSet(s);
			});

			// add the source set to the sourceSets extensions so that we can refer to the source set by its
			// name inside the DSL of sourceSets {}
			sourceSets.getExtensions().getExtraProperties().set(s.getName(), s);

			// setup dist task for each source set if this is distributing
			configureDistTaskForSourceSet(s);
		});

		// when distDev and playframework is applied
		// we need to create the distDev task and make it depends on the distDevTask
		if (isPlayApplied(p) && isDistDev()) {
			configureDistDevTaskForPlayFrameWorkMainSrcSet();
		}

	}

	private void configureDistDevTaskForPlayFrameWorkMainSrcSet() {
		Task distDevTask = p.task(distTaskPrefix());
		distDevTask.setDescription("Distributing play framework under dev mode with all dependencies for source set main. This task depends on actual play dist task.");
		distDevTask.setGroup(DIST_TASK_GROUP);
		if (distAll != null) distAll.dependsOn(distDevTask);
		distDevTask.dependsOn("dist");
	}

	private void configureDistTaskForSourceSet(SourceSet s) {

		Jar jarTask;
		Copy copyTask;
		Task distTask;

		if (s.getName().equals("main")) {
			Jar t = (Jar)p.getTasks().findByName("jar");
			if (jarAppendix != null && jarAppendix.length() > 0)
				t.getArchiveAppendix().set(jarAppendix);

			jarTask = t;

			Copy c = p.getTasks().create("copyLibs", Copy.class);

			Configuration runTimeCol = p.getConfigurations().findByName("runtimeClasspath");
			if (runTimeCol == null) runTimeCol = p.getConfigurations().findByName("runtime");	// backward compatability
			c.from(runTimeCol);

			copyTask = c;

			distTask = p.task(distTaskPrefix());

			distTask.setDescription("Distributing jar together with all dependencies for source set main");
		} else {
			Jar t = (Jar)p.getTasks().findByName(String.format("%sJar", s.getName()));

			if (jarAppendix != null && jarAppendix.length() >0) {
				t.getArchiveAppendix().set(s.getName() + "-" + this.jarAppendix);
			}
			else t.getArchiveAppendix().set(s.getName());

			jarTask = t;

			Copy c = p.getTasks().create(String.format("copy%sLibs",Utils.capitalize(s.getName())), Copy.class);

			Configuration runTimeCol = p.getConfigurations().findByName(String.format("%sRuntimeClasspath", s.getName()));
			if (runTimeCol == null) runTimeCol = p.getConfigurations().findByName(String.format("%sRuntime", s.getName()));	// backward compatability
			c.from(runTimeCol);

			copyTask = c;

			distTask = p.task(distTaskPrefix()+Utils.capitalize(s.getName()));

			distTask.setDescription("Distributing jar together with all dependencies for source set "+s.getName());
		}

		jarTask.getDestinationDirectory().set(new File(p.getBuildDir().getAbsolutePath()+"/"+DIST_FOLDER+"/"+s.getName()));
		copyTask.setDestinationDir(new File(p.getBuildDir().getAbsolutePath()+"/"+DIST_FOLDER+"/"+s.getName()+"/libs"));
		copyTask.exclude("**/*.class");
		copyTask.setIncludeEmptyDirs(false);

		distTask.dependsOn(jarTask, copyTask);

		distTask.setGroup(DIST_TASK_GROUP);

		if (distAll != null) distAll.dependsOn(distTask);
	}



	public void addLanguage(LanguageConfiguration config) {
		langConfigs.put(config.getName(), config);
		config.configure(this);
		if (!sourceSetsSetup) setupSourceSets();

		// TODO: setting up buildAll task which depends on all build tasks of individual source set.
	}

	/**
	 * Apply a certain plugin to the project
	 * @param id the id of the plugin
	 */
	public void applyPlugin(String id) {
		p.getPluginManager().apply(id);
	}

	public Project getProject() {
		return p;
	}

	protected void constructDsl() {
		ExtensionAware hnct = (ExtensionAware)p.getExtensions().create(DslHnct.CMD, DslHnct.class, this);

		ExtensionAware lang = (ExtensionAware)hnct.getExtensions().create(DslLang.CMD, DslLang.class, this);

		// this has been deprecated
		sourceSets = (ExtensionAware)hnct.getExtensions().create(DslSourceSets.CMD, DslSourceSets.class, this);

		// dsl to configure repo for dependency
		ExtensionAware repo = (ExtensionAware)hnct.getExtensions().create(DslRepo.CMD, DslRepo.class, this);

		RepositoryHandler snapshotRepoHandler = this.publicationServices.createRepositoryHandler();
		RepositoryHandler productionRepoHandler = this.publicationServices.createRepositoryHandler();

		// publishing repository is only configured in the root projects and will be available
		// to all other projects within the build. This is to ensure consistency when publishing
		// project dependencies. To understand it better, we explain it as following:
		// Let say we have a project A and a project B within the build where A has a source set dependency on B in dev mode.
		// during development, let say, we want to publish A so that other people can use it. Since A is dependent on B, it is
		// logical that B should also be published so that the consuming project can run.
		// To do this, B needs to be PUBLISHED TO THE SAME REPOSITORY so that the consuming project can find B WHENEVER it can find A.
		// This "same repo publishing" restriction will reduce errors especially when the dependency chain among project is a little bit deep or wide.
		// Because of this, if B is configured with a different publishing repository than A it will be come very messy. Hence we only allow configuring
		// common publishing repository for all projects within the build.
		// This also aligns with our goal that publishing or dependency repositories lies at the root repo, and individual project only concern with
		// implementation and dependencies on other projects.
		if (p.getRootProject() == p) {

			// The publish dsl allows configuring snapshot and production repositories for publishing dev and production publication respectively.
			// dev publication typically will contains snapshot versioning and commit hash. It will also include all project dependencies, source set dependencies as artifacts
			// Production publication will contain version number, no commit hash. It doesn't allow publication with project or source set dependencies.
			ExtensionAware publish = (ExtensionAware) hnct.getExtensions().create(DslPublish.CMD, DslPublish.class, this, this.p, snapshotRepoHandler, productionRepoHandler);


		}

		ExtensionAware dependencyInternal = (ExtensionAware)((ExtensionAware)p.getDependencies()).getExtensions().create(
				DslInternalDependency.CMD, DslInternalDependency.class,this
		);

		sourceSets = (ExtensionAware)((ExtensionAware)p.getDependencies()).getExtensions().create(
				DslSourceSets.CMD, DslSourceSets.class, this
		);
	}

	/**
	 * Check if the current gradle build is doing a distribution
	 *
	 * This check is useful for example, to prevent accessing snapshot repository during distribution, so that
	 * user can't distribute snapshot to production.
	 *
	 * @return true if the project is distributing
	 */
	public boolean isDistributing() {
		StartParameter sps = getProject().getGradle().getStartParameter();
		Iterator<String> it = sps.getTaskNames().iterator();
		while (it.hasNext()) {
			if (it.next().contains("dist")) return true;
		}

		return false;
	}

	public boolean isDistDev() {
		StartParameter sps = getProject().getGradle().getStartParameter();
		Iterator<String> it = sps.getTaskNames().iterator();
		while (it.hasNext()) {
			if (it.next().contains("distDev")) return true;
		}

		return false;
	}

	public boolean isPlayApplied(Project p) {
		if (p.getPluginManager().hasPlugin("play") || p.getPluginManager().hasPlugin("org.gradle.playframework"))
			return true;
		else return false;
	}

}
