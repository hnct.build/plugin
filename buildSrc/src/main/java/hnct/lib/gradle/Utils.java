package hnct.lib.gradle;

import org.gradle.api.tasks.SourceSetContainer;
import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.bundling.Jar;

import javax.xml.transform.Source;
import java.util.Optional;

public class Utils {

    public static String capitalize(String s) {
        return s.substring(0,1).toUpperCase() + s.substring(1);
    }

    public static SourceSet findSourceSetMatchConfig(Project dp, String conf) {

        SourceSetContainer srcSets = dp.getExtensions().getByType(SourceSetContainer.class);

        Optional<SourceSet> s = srcSets.stream().filter(src -> conf.contains(src.getName())).findAny();

        if (s.isPresent()) return s.get();
        else if (conf.equals("implementation") || conf.equals("api") || conf.equals("runtime") ||
            conf.equals("compile") || conf.equals("runtimeClasspath") || conf.equals("compileClasspath")){
            return srcSets.getAt("main");
        } else return null;
    }

    // make a configuration consumable, this relies on the fact that there is a distribution task
    // configured for the project P and source set s in advance. This is done at the HNCTPlugin class
    public static void makeConsumable(Project p, Configuration config, SourceSet s) {
        config.setCanBeConsumed(true);

        if (!s.getName().equals("main"))
            // adding artifacts for the sourceset configurations
            p.artifacts(a -> a.add(config.getName(),
                    p.getTasks().findByName(
                            String.format("jar%s", Utils.capitalize(s.getName()))
                    ))
            );

        else if (s.getName() == "main")
            p.artifacts(a -> a.add(config.getName(), p.getTasks().findByName("jar")));
    }

    /**
     * Find a configuration in the dependency project for source set dependencySrcSet that has the same
     * type as the configuration in this project, represented by confName
     *
     * For example, if we declaring dependency for configuration testImplementation
     * and it is depending on the source set thrift of the dependency project, then
     * the correct configuration to be retrieved is thriftImplementation.
     *
     * @param currentProject the project that own the confName
     * @param dependencyProject the project contains the sourceset to be dependent upon
     * @param confName the configuration name of the project that has dependency on the dependency project
     * @param dependencySrcSet the source set that will be depended upon
     * @return the configuration matching the configuration specified by confName, if any, else,
     * the implementation configuration of the dependency srcSet is returned
     */
    public static Configuration findMatchingConfiguration(Project currentProject, Project dependencyProject, String confName, SourceSet dependencySrcSet) {

        SourceSet dependent = findSourceSetMatchConfig(currentProject, confName);

        // normally, for java, we can select the correct type of configuration
        // but for other system, such as playframework, the confName = play, and this
        // kind of configuration doesn't have such api / implementation / runtime etc... defined on it
        // We also don't hope, in this case, to find the dependent sourceset corresponding the play configuration
        if (dependent != null) {
            if (dependent.getApiConfigurationName().equals(confName))
                return dependencyProject.getConfigurations().getAt(dependencySrcSet.getApiElementsConfigurationName());
            else if (dependent.getImplementationConfigurationName().equals(confName))
                return dependencyProject.getConfigurations().getAt(dependencySrcSet.getRuntimeElementsConfigurationName());
            else if (dependent.getCompileClasspathConfigurationName().equals(confName))
                return dependencyProject.getConfigurations().getAt(dependencySrcSet.getApiElementsConfigurationName());
            else if (dependent.getRuntimeClasspathConfigurationName().equals(confName))
                return dependencyProject.getConfigurations().getAt(dependencySrcSet.getRuntimeElementsConfigurationName());
//            else if (dependent.getCompileConfigurationName().equals(confName))
//                return dependencyProject.getConfigurations().getAt(dependencySrcSet.getCompileConfigurationName());
//            else if (dependent.getRuntimeConfigurationName().equals(confName))
//                return dependencyProject.getConfigurations().getAt(dependencySrcSet.getRuntimeConfigurationName());
            else if (dependent.getRuntimeOnlyConfigurationName().equals(confName))
                return dependencyProject.getConfigurations().getAt(dependencySrcSet.getRuntimeElementsConfigurationName());

        }

        // in any case, return the runtime classpath configuration as it would be the safetest and contain all necessary
        // dependency for both runtime and compile time.
        return dependencyProject.getConfigurations().getAt(dependencySrcSet.getImplementationConfigurationName());
    }

    /**
     * Find a source set of a project given its sourceset name or configuration name
     * @param p project which has the source set
     * @param sn the source set name
     * @return the source set object
     */
    public static SourceSet findSourceSet(Project p, String sn) {

        SourceSetContainer srcSets = p.getExtensions().getByType(SourceSetContainer.class);

        SourceSet s = srcSets.findByName(sn);

        if (s != null) return s;

        // backward compatibility, find sourceset corresponding to a configuration
        Configuration c = p.getConfigurations().findByName(sn);

        if (c == null) throw new RuntimeException("Unable to find a source set or a configuration with name "+
                sn+" in project "+ p.getPath());

        s = Utils.findSourceSetMatchConfig(p, sn);

        return s;
    }

    public static Configuration findRuntimeConfig(SourceSet s, Project p) {

        Configuration c = s.getName().equals("main")?
                p.getConfigurations().getAt("runtimeClasspath"):
                p.getConfigurations().getAt(s.getName()+"RuntimeClasspath");

        return c;
    }

    public static Jar findJarTask(Project p, SourceSet s) {
        if (s.getName().equals("main"))
            return (Jar)p.getTasks().findByName("jar");
        else return (Jar)p.getTasks().findByName(String.format("jar%s", Utils.capitalize(s.getName())));
    }
}
