package dep_injection

import com.google.inject.AbstractModule

/**
 * @author Ryan
 * 
 * The Fudivery module to wire up everything
 * 
 */
class Main extends AbstractModule {

	def configure(): Unit = {
	}

}