package controllers

import com.google.inject.Inject
import javax.inject.Singleton
import play.api.Configuration
import play.api.mvc.{AbstractController, ControllerComponents}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

import views._

import additional.AdditionalJavaClass
/**
 * @author Ryan
 */
@Singleton
class Application @Inject()
(
	cc : ControllerComponents,
	config : Configuration
) extends AbstractController(cc) {

	def index(path: String) = Action.async { implicit playHttpRequest =>

		//new AdditionalClass();

 	  Future.successful(Ok(html.index.render()))
	}

}