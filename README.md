# Introduction
This Gradle plugin provide syntax to help setup a local development structure easily through a set of configuration syntax that allows

- Easily specification of languages to be used, e.g. Scala or Java
- Dependency configuration among project source sets
- Dependency configuration among source sets of different projects
- Distribution of project source sets that includes all dependencies (whether it is external or another source sets from the same project or different projects)
- Include GIT Commit hash into the distributed jar
- Easy configuration of dependency repository
- Publishing work flow for production and development

The code include examples in the pluginTest and pluginTestDependencyProject. Some explanation of syntax can also be found in the source code.

# Using the plugin

Assuming you have a project structure as following

<pre>

root
|- P1
|- P2

</pre>

## Configure dependency repositories

To include standard repository into all projects, inside the root project:

``` gradle
hnct {
    repositories {
        standard()
    }
}
```

The `repositories` dsl also allow configuring custom repositories:

``` gradle

hnct {
    repositories {
        custom {
            type = "ivy"
            url = "...."
            username = "..."
            password = "..."
        }
    }

}

```

By default, any configured repositories are applied to subproject as well. If you don't want this and want to configure repositories for subprojects individually add `appliedToChildren = false` to the `custom` block.

## Configure languages to be used

Currently the plugin support scala and java and wrap around the corresponding language plugin. Suppose you want to use Java with project `P1` and Java and Scala for project `P2`

In `P1 build.gradle` add

```
hnct {
    lang {
        java()
    }
}
```

In `P2 build.gradle` add

```
hnct {
    lang {
        scala version:"2.12.1"
        java()
    }
}
```

The plugin will handle adding the scala library to the build classpath so that scala plugin can compile scala code. It uses the default zinc compiler.

## Configure source set dependencies

Let say project `P2` has a few source sets name `p201`, `p202` and main. The dependencies among source sets can be configured using source set DSL:

``` gradle
// DEPERECATED SINCE 1.3.0
/* hnct {
    lang {
        scala version:"2.12.1"
        java()
    }

    sourceSets {
        dep p202, main, p201
    }
} */

// SINCE 1.3.0 USE

dependencies {
    sourceSets {
        p202Implementation main, p201
    }
}

```

This configuration allow `p202Implementation` to depends on `main` and `p201` source sets. Underlying, the
`implementation` of `p202` will be made to dependent on the **matching configuration** of `main` and `p201`. In the cases
of matching configuration can't be found, `implementation` configuration will be used. The two sourceset `main` and `p201`
will also be made consumable and publish a jar (through adding artifact to the configuration
https://docs.gradle.org/current/dsl/org.gradle.api.artifacts.dsl.ArtifactHandler.html). When published, the module descriptor (ivy.xml)
of `p202` will include references to its dependency source sets.

#### Source set output dependency

There is another way of using another source set as dependency is to configure dependency using source set output. For example

``` gradle

dependencies {
    sourceSets {
        p202Implementation main.output
    }
}

```

Using output dependency, we explicitly said that the p202 implementation depends on the classes file of the main source set, the
main source set classes therefore, will be included in the p202 jar during publication or dist. The dependencies collected in the main
source set matching configuration (in this case, `implementation` configuration) will also be included as dependencies of `p202Implementation`.
Underlying, it is actually equivalent to

```gradle

dependencies {
    p202Implementation.extendsFrom implementation
}

```

This is to ensure that when `p202` is used anywhere, the dependencies are included correctly. In the case `implementation` configuration
in turn depends on another source set output, like for example,

```gradle

dependencies {
    sourceSets {
        p202Implementation main.output
        implementation p201.output
    }
}

```

The output classes of p201 will be recursively included into the `p202` jar and the dependencies configured for p201Implementation
will also be recursively added to p202Implementation.

**Note:** You can not configured source set output of a different project as dependency.

## Configure internal project dependencies

Let say that project `P1` has source set named `p101` and `main`. If we want to configure `P1` `main` source set to depends on the `main` source set from `P2`, we can do as following in project `P1`:

``` gradle
dependencies {
    internal {
        implementation ':P2', 'group:P2:version', true
    }
}
```

The configuration for a dependency has three component:

- `:P2` this states that the `implementation` configuration is dependent on the main source set from project `P2`
- `group:P2:version` This is the usual dependency notation. It specifies the jar that the `implementation` configuration depends on if the local project dependency is not used.
- `true` the flag can have either true or false value. If it is false, the jar notation will be used, and if it is true, the source set / project notation will be used.

The main use case of this is when you are developing multiple project locally, you can set the flag to `true` so that you don't have to publish them everytime code is changed. When coding is completed, the flag can be changed to `false` to consume the published version of individual project.

You can also configure a specific source set of a project to depends on other specific source sets from another project for example, `p101` depends on `p202` and `p201`

``` gradle
dependencies {
    internal {
        p101Implementation([id: ':P2', src:['p202','p201']]), 'group:P2:version', true
    }
}
```

In this scenenario, the type of configuration selected for `p202` and `p201` will be the same as the `p101`, in this case, `p202Implementation` and `p201Implementation` configuration will be selected. The selected configuration will be made consumable and a jar artifact (of the sourcesets `p202` and `p201`) will be added for these configuration.

## Distribution tasks

The plugin pre-configure some tasks to distribute jar files. For each source set there is one dist[SourceSetName] task for distributing the source set.

To distribute everything in single jar and with all dependencies copied, use `distAll`

To distribute `main` source set use `dist` task

## Publishing tasks

### Publish artifacts

We distinguish between two types of publishing:

* Publishing development artifacts
* Publishing production artifacts

#### Configuration

Before you can publish anything, it is necessary that you configure target repositories

The configuration of target repositories is ONLY ALLOWED at the ROOT projects. This limitation ensure that we don't accidentally publish a project to one repo, and its dependency projects to another one. This will cause difficulty and confusion when consuming these published artifacts.

To configure target dependency, in the root project:
``` gradle
hnct {
    publish {
        dev {
            ivy {
                // .... normal repository configuration
            }

            // ....multiple repos possible...
        }

        prod {
            ivy {
                // ....
            }

            // ....
        }
    }
}
```

The `prod` and `dev` DSL is used to configure production and development repositories respectively. 

Once there are repositories configured, for each sub project, there will be two tasks `publishDev` and `publishProd`. These tasks publish all publication, one for each source set, to all configured repositories.

To publish all artifacts to a particular repo there will be task: `publish[Dev/Prod]To[RepoName]Repo`

To publish a particular artifact a source set to a particular repo, there will be task: `publish[Dev/Prod][SourceSetName]To[RepoName]Repo`

#### Note on publishing development artifacts

The version of the published artifacts will include current date string, and a hash of the commit if git is used.

It is recommended to run `publishDev` on all projects so that if a project depends on other projects, all its dependencies will be published too.

#### Note on publishing production artifacts

Publishing production artifact to production repo will encounter exception if there are project dependencies. Production should always use jar dependencies.

The version is the version of the project.


### Publish artifacts for deployment

Besides the publishing artifacts on the above section, we provide another type of publish the artifact which focuses on controlling the dependency's version of the artifact.

**Note**: This type of publishing `support the Ivy format only`

> *We will focus this publish type to end-users application projects, since the issue cause higher impact with transitive dependency version to these projects rather than libraries used by other application*

#### **What is the issue?**

Not only library projects, but we also want to publish the artifact of applications that will be deployed to the server. The reason for this is helping save a backup version and restore it easier without re-build. 

However, we know that some applications may require a specific version of dependency to run as expected. With this requirement, it leads to an issue **how can we control transitive dependency/dependency's versions of the artifact**.

With the original way, we can use [`configurations.all.resolutionStrategy.force`](https://docs.gradle.org/current/dsl/org.gradle.api.artifacts.ResolutionStrategy.html#org.gradle.api.artifacts.ResolutionStrategy:force\(java.lang.Object\[\]\)) to control the version of all transitive dependencies/dependencies version to get the exact dependencies we want for the build process and copy the built file to the server for deployment. However, it's difficult for controlling a published artifact for future deployment, because **Ivy module haven't support the function to force version of all transitive dependencies/dependencies as the gradle does**

Therefore, when we download the artifact with gradle then deploy to server, the dependencies version may be upgraded due to gradle behavior. 

>"Gradle will consider all requested versions, wherever they appear in the dependency graph. Out of these versions, it will select the highest one."
>
> - Refference at [Understanding dependency resolution](https://docs.gradle.org/current/userguide/dependency_resolution.html#:~:text=Given%20a%20required%20dependency%2C%20with,metadata%20files%20describing%20the%20module%20\(%20.)


#### **How can this solve the issue?**

By adding all resolved dependencies to the artifact descriptor with the new configuration (`deploy`). 
This will contain exact list of dependencies and it's version that the application need for the deployment.

> **Note**: Deploy configuration will have attribute "transitive = false"

#### **What is the difference compared with the original publish artifact?** 

Basically, this will similar with the original publishing, we just adding another config mode for the artifact descriptor. 

- The artifact descriptor will have **default** configuration, similar with the original artifact publiation for normal usage 
- Another configuration is `deploy` configuration, when selecting this. Gradle will download exact dependencies of that configuration, which mean no resolve for transitive dependencies and upgrading publish version 

#### **How to use?**

Similarly to the original publish artifact, We distinguish between two types of publishing:

* Publishing development artifacts for deployment: `publishDeployDev`
* Publishing production artifacts for deployment: `publishDeployProd`

# Using with IDEA

It is recommended to use the plugin with IDEA through direct importing gradle file into IDEA. Using the `gradle idea` command is possible but without multiple
sourcesets supports, hence, internal dependency on specific sourcesets is not possible with this approach.

For multiple sourcesets setup in IDEA, you need to import the root project gradle file with option "Create separate module per source set" selected.

# Playframework support

Example of using with playframework is available in the `playTest` example project within this repository. It is possible
to configure play dependency using `play` configuration. Normal internal sourceset dependency is possible too.

However, since importing with "Create separate module per source set" option doesn't recognize the `app` sourceset as it is,
, but is recognized as part of a `main` sourceset of the JAVA Model, you need to additionally add

``` gradle
configurations {
    implementation.extendsFrom play
}
```

So that the `main` source set can see the dependencies from `play` configuration.

# Example project

There are two projects included in this repository for example.