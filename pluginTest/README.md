#READ ME

This project is for testing following features of the plugin:

* Applying plugin in a sub-project
* Configuring languages that the project is coded in (e.g. java and scala at the same time)
* Configuring additional source sets for the project
* Configuring dependencies among source sets.
* 