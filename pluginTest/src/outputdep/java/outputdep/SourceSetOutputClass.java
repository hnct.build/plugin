package outputdep;

/**
 * This class is created to demonstrate that its class file will
 * be included into the jar of the depending source set, which configure
 * outputdep as sourceset output dependency.
 */
public class SourceSetOutputClass {

    public void print() {
        System.out.println("Hello Source Set Output Dependency.");
    }

}
