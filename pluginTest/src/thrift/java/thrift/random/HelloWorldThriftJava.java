package thrift.random;

import random.HelloWorldJava;
import common.random.HelloWorldCommonJava;
import common.random.HelloWorldCommonScala;

import dependency.VeryImportantLibrary;     // demonstrate project dependency.

public class HelloWorldThriftJava {

    public static String message = "HELLO WORLD";

}
